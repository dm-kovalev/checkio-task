# CheckIO-Task

## Getting the source
```
$ git clone https://bitbucket.org/dm-kovalev/checkio-task
$ cd checkio-task
```

## Building
```
$ ./gradlew build
```

## Usage
```
$ ./gradlew run -Pargs=N
```