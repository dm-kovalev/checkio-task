import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.*;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import apiwrapper.StepikService;
import apiwrapper.data.*;

public class StepikCrawler {
    private static final String BASE_URL = "https://stepik.org/api/";
    private final StepikService stepikApiService;

    public StepikCrawler() {
        Gson customNamingGson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(customNamingGson))
                .build();
        stepikApiService = retrofit.create(StepikService.class);
    }

    public List<Course> getAllCourses() {
        List<Course> courses = new LinkedList<>();
        int page = 1;
        boolean hasNext = true;

        while (hasNext) {
            Response<CoursesPageResponse> response = null;
            try {
                 response = stepikApiService.getCoursesPage(page).execute();
            }
            catch (IOException e) {
                System.out.println("Unsuccessful request to the Stepik-API: \n"
                        + e.getMessage());
            }
            if (response != null && response.body() != null) {
                CoursesPageResponse currentPage = response.body();
                courses.addAll(currentPage.getCourses());
                hasNext = currentPage.isHasNext();
            }
            page++;
        }

        return courses;
    }
}
