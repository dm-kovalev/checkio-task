import java.util.Comparator;
import java.util.List;
import apiwrapper.data.Course;

public class Main {
    private static void printMostPopularCourses(List<Course> courses, int n) {
        System.out.printf("%-60s%8s%n%n", "Title", "Learners");
        courses.stream()
                .sorted(Comparator.comparing(Course::getLearnersCount).reversed())
                .limit(n)
                .forEach(c -> System.out.printf("%-60s%8d%n", c.getTitle(), c.getLearnersCount()));
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            try {
                int n = Integer.parseInt(args[0]);
                printMostPopularCourses((new StepikCrawler()).getAllCourses(), n);
            }
            catch (NumberFormatException e) {
                System.out.println("Incorrect parameter format");
            }
        }
        else {
            System.out.println("Incorrect number of args");
        }
    }
}
