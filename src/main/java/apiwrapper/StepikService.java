package apiwrapper;

import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.Call;

import apiwrapper.data.CoursesPageResponse;

public interface StepikService {
    @GET("courses")
    Call<CoursesPageResponse> getCoursesPage(@Query("page") int pageNum);
}
