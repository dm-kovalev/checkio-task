package apiwrapper.data;

import java.util.ArrayList;
import java.util.List;

public class CoursesPageResponse {

    private Meta meta;
    private List<Course> courses;

    public CoursesPageResponse() {
        courses = new ArrayList<>();
    }

    public List<Course> getCourses() {
        return courses;
    }

    public boolean isHasNext() {
        return meta.isHasNext();
    }
}
