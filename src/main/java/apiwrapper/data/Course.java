package apiwrapper.data;

public class Course {
    private String title;
    private int learnersCount;

    public String getTitle() {
        return title;
    }

    public int getLearnersCount() {
        return learnersCount;
    }
}
